package rs.mpele.pHStat.Webcam;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import net.miginfocom.swing.MigLayout;
import rs.mpele.pHStat.Podesavanja;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Dimension;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.Component;
import javax.swing.Box;

public class CalibrationCammeraLive extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	private MarvinImagePanel marvinPanel;
	private JLabel lblKorak;
	private JTextField textFieldKorak;
	private JPanel panelKomande;
	private JButton btnDole;
	private JButton btnLevo;
	private JButton btnDesno;
	private JLabel lblNewLabel;
	private OcitajPH mOcitajPH;
	private JSpinner spinnerTreshold;
	private JButton btnProsiri;
	private JButton btnSkupi;
	private JButton btnIzduzi;
	private JButton btnSkupi_1;
	private JButton btnNewButton_1;
	private JButton btnUcitajTreshold;
	private JLabel lblDistanceBetweenDigits;
	private JSpinner spinnerRelativnoRastojanje;
	private Component horizontalStrut;


	/**
	 * Create the frame.
	 * @param ocitajPH 
	 * @throws IOException 
	 */
	public CalibrationCammeraLive(OcitajPH ocitajPH) throws IOException {
		this.mOcitajPH = ocitajPH;
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(500, 100, 680, 670);
		setTitle("Kalibracija Live");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[]", "[][][][]"));

		new JLabel("");
		marvinPanel = new MarvinImagePanel();
		marvinPanel.setMinimumSize(new Dimension(600, 480));
		contentPane.add(marvinPanel, "cell 0 0");

		panelKomande = new JPanel();
		panelKomande.setLayout(new MigLayout("", "[][][grow]", "[][]"));

		lblKorak = new JLabel("Step");
		panelKomande.add(lblKorak, "cell 0 0,alignx trailing");

		textFieldKorak = new JTextField();
		textFieldKorak.setText("5");
		panelKomande.add(textFieldKorak, "cell 1 0,growx");
		textFieldKorak.setColumns(4);

		btnLevo = new JButton("Left");
		btnLevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setX((mOcitajPH.getX()-Integer.valueOf(textFieldKorak.getText())));
			}
		});
		panelKomande.add(btnLevo, "cell 0 1");

		JButton btnGore = new JButton("Up");
		panelKomande.add(btnGore, "flowy,cell 1 1");

		btnDole = new JButton("Down");
		btnDole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setY(mOcitajPH.getY()+Integer.valueOf(textFieldKorak.getText()));
			}
		});
		panelKomande.add(btnDole, "cell 1 1");

		btnDesno = new JButton("Right");
		btnDesno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setX((mOcitajPH.getX()+Integer.valueOf(textFieldKorak.getText())));
			}
		});
		panelKomande.add(btnDesno, "flowx,cell 2 1");

		lblNewLabel = new JLabel("Treshold");
		panelKomande.add(lblNewLabel, "flowx,cell 2 0");

		spinnerTreshold = new JSpinner();
		spinnerTreshold.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				mOcitajPH.getPrepoznavanjeCifre().setTreshold((Integer) spinnerTreshold.getValue());
			}
		});
		spinnerTreshold.setModel(new SpinnerNumberModel(mOcitajPH.getPrepoznavanjeCifre().getTreshold(), null, null, new Integer(1)));

		panelKomande.add(spinnerTreshold, "cell 2 0");
		btnGore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setY(mOcitajPH.getY()-Integer.valueOf(textFieldKorak.getText()));
			}
		});
		contentPane.add(panelKomande, "cell 0 1,grow");

		btnProsiri = new JButton("Expand");
		btnProsiri.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setSirina(mOcitajPH.getSirina()+Integer.valueOf(textFieldKorak.getText()));
			}
		});
		
		horizontalStrut = Box.createHorizontalStrut(50);
		panelKomande.add(horizontalStrut, "cell 2 1");
		panelKomande.add(btnProsiri, "cell 2 1");

		btnSkupi = new JButton("Shrink");
		btnSkupi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setSirina(mOcitajPH.getSirina()-Integer.valueOf(textFieldKorak.getText()));
			}
		});
		panelKomande.add(btnSkupi, "cell 2 1");

		btnIzduzi = new JButton("Extend");
		btnIzduzi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setVisina(mOcitajPH.getVisina()+Integer.valueOf(textFieldKorak.getText()));
			}
		});
		panelKomande.add(btnIzduzi, "cell 2 1");

		btnSkupi_1 = new JButton("Short");
		btnSkupi_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOcitajPH.setVisina(mOcitajPH.getVisina()-Integer.valueOf(textFieldKorak.getText()));
			}
		});
		panelKomande.add(btnSkupi_1, "cell 2 1");
		
		btnUcitajTreshold = new JButton("Load treshold");
		btnUcitajTreshold.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spinnerTreshold.setValue(mOcitajPH.getPrepoznavanjeCifre().getTreshold());
				spinnerRelativnoRastojanje.setValue(mOcitajPH.getPrethodnaTackaRelativno());
			}
		});
		panelKomande.add(btnUcitajTreshold, "cell 2 0");
		
		lblDistanceBetweenDigits = new JLabel("Distance between digits");
		panelKomande.add(lblDistanceBetweenDigits, "cell 2 0");
		
		
		double current = ocitajPH.getPrethodnaTackaRelativno();
		double min = 1.0;
		double max = 10.0;
		double step = 0.01;
		spinnerRelativnoRastojanje = new JSpinner(new SpinnerNumberModel(current, min, max, step));
		spinnerRelativnoRastojanje.setMaximumSize(new Dimension(60, 20));
		spinnerRelativnoRastojanje.setMinimumSize(new Dimension(50, 20));
		spinnerRelativnoRastojanje.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				mOcitajPH.setPrethodnaTackaRelativno( (Double) spinnerRelativnoRastojanje.getValue());
			}
		});
		panelKomande.add(spinnerRelativnoRastojanje, "cell 2 0");

		btnNewButton_1 = new JButton("Save");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Podesavanja podesavanja = new Podesavanja();
				try {
					podesavanja.ucitajPodesavanja();
					podesavanja.snimiPodesavanjaZaKameru(mOcitajPH.getX(),
							mOcitajPH.getY(),
							mOcitajPH.getPrepoznavanjeCifre().getTreshold(),
							mOcitajPH.getSirina(),
							mOcitajPH.getVisina(),
							mOcitajPH.getPrethodnaTackaRelativno());
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		contentPane.add(btnNewButton_1, "cell 0 2,alignx right");

	}

	public void setSlika(MarvinImage marvinImage) throws IOException{
		marvinPanel.setImage(marvinImage);
		//	lblOriginalnaSlikaLabel.setIcon(new ImageIcon(marvinImage.getScaledInstance(400, 300, Image.SCALE_SMOOTH)));
	}
}
